def check_structure(data: dict, schema: dict):
    """
    :param data: data of response
    :param schema: schema for data validation
    :return: return True if data matches to schema, return False if not
    """
    for key, value in data.items():
        if type(data[key]).__name__ not in schema[key]:
            return False

    return True
