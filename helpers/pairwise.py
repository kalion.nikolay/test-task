from allpairspy import AllPairs


class Pairwise:
    def __init__(self, pairs: list, filtration: tuple = None):
        """
        :param pairs: list with lists of pairs which we need to pairwise.
        e.g. [[123, 3221], ['abc', 'cbd']]
        :param filtration: which pair we need to skip. format: ('type of validation', 'invalid pair').
        available validations: by type.
        e.g. ('by type', [(int, float), (int, float), (bool,)])
        """
        self.pairs = pairs
        self.filtration = filtration

    def by_type(self, pairs):
        combination = []
        for i, pair in enumerate(pairs):
            if type(pair) in self.filtration[1][i]:
                combination.append(True)
            else:
                combination.append(False)
        if combination == [True for _ in self.filtration[1]]:
            return False
        else:
            return True

    def generate_pairwise(self):
        if self.filtration:
            for pair in AllPairs(self.pairs, filter_func=eval(f'self.{self.filtration[0]}')):
                yield pair
        else:
            for pair in AllPairs(self.pairs):
                yield pair
