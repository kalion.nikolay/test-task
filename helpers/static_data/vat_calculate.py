class Errors:

    def __init__(self):
        self.invalid_data = 'invalid data'
        self.invalid_sum_or_vat_percent = {'status': 'error', 'data': 'invalid data', 'message': 'Sum or vat percent cannot be < 0'}