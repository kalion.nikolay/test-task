import pytest
from helpers.pairwise import Pairwise


def pairwise_generator():
    parameters = [
        ['over9000', 10, 10.5, True, [], {}, None],
        ['over9000', 10, 10.5, True, [], {}, None],
        ['over9000', 10, 10.5, True, [], {}, None]
    ]
    pairwise = Pairwise(parameters, filtration=('by_type', [(int, float), (int, float), (bool,)]))
    for pair in pairwise.generate_pairwise():
        yield pair


@pytest.fixture(scope='function', params=pairwise_generator())
def types_pairwise(request):
    yield request.param
