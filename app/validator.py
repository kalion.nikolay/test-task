def validator(validation: dict, request_data):
    """
    :param validation: validation schema. must be dict with tuples
    e.g.
    {'key1': (int, float),
     'key2': (int, float),
     'key3': (bool,)}
    :param request_data: data for which must be validated
    :return: True if validation OK, return {'error': 'invalid data', 'message': {[key1 must be {str}, but get {float}]}
    """
    data = []

    for key, validation_types in validation.items():
        if type(request_data.get(key, None)) not in validation_types:
            data.append(f'{key} must be {validation_types}, but get {type(request_data.get(key, None))}')

    if data:
        return {'status': 'error', 'message': 'invalid data', 'data': data}
    else:
        return True
