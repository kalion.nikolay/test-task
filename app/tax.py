from .validator import validator


def vat_calculate(request_data):
    request_validation = {'money_sum': (int, float),
                          'vat_percent': (int, float),
                          'vat_included': (bool,)}
    validation = validator(request_validation, request_data)

    if validation is not True:
        return validation

    if request_data.get('money_sum') > 2147483647:
        return {'status': 'error', 'data': 'you really have that much money?', 'message': 'invalid data'}

    if request_data.get('money_sum') < 0 or request_data.get('vat_percent') < 0:
        return {'status': 'error', 'data': 'Sum or vat percent cannot be < 0', 'message': 'invalid data'}

    if request_data['vat_included']:
        total_without_vat = request_data.get('money_sum') / (100 + request_data.get('vat_percent')) * 100
        vat = request_data.get('money_sum') - total_without_vat
        total = request_data.get('money_sum')
    else:
        total_without_vat = request_data.get('money_sum')
        vat = request_data.get('money_sum') / 100 * request_data.get('vat_percent')
        total = request_data.get('money_sum') + vat

    return {'status': 'success', 'data': {'total': f'${total:.2f}', 'vat': f'${vat:.2f}',
                                          'total_without_vat': f'${total_without_vat:.2f}'}}
