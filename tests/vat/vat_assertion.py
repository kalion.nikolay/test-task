# For typical VAT assertion. Repeating business logic for easier support in future and dynamic test data generation.
# Imagine what we really doing request for endpoint. Not calling method from nearby file.
def vat_assertion(request_data: dict):
    if request_data.get('vat_included'):
        total_without_vat = request_data.get('money_sum') / (100 + request_data.get('vat_percent')) * 100
        vat = request_data.get('money_sum') - total_without_vat
        total = request_data.get('money_sum')
    else:
        total_without_vat = request_data.get('money_sum')
        vat = request_data.get('money_sum') / 100 * request_data.get('vat_percent')
        total = request_data.get('money_sum') + vat

    return {'status': 'success', 'data': {'total': f'${total:.2f}', 'vat': f'${vat:.2f}',
                                          'total_without_vat': f'${total_without_vat:.2f}'}}