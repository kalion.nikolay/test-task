import pytest
import random
from app.tax import vat_calculate
from helpers.static_data.vat_calculate import Errors
from tests.vat.vat_assertion import vat_assertion
from helpers.stucture_checker import check_structure

"""
#1 VAT calculate positive test

Steps:
1. Fill field {money} by random value >1 
2. Fill field {vat_percent} by random value 1-100
3. For test when VAT included in sum field {vat_included} must be True
4. Assert with function vat_assertion()

Expected result:
Assert must be passed
"""


@pytest.mark.parametrize("vat_included", [True, False])
def test_vat_calculate(vat_included):
    data = {'money_sum': random.randint(1, 500), 'vat_percent': random.randint(1, 100), 'vat_included': vat_included}

    actual_result = vat_calculate(data)
    expected_result = vat_assertion(data)

    assert expected_result == actual_result


"""
#2 Check boundary values for money and vat_percent

Steps:
1. {money} must be >= 0
2. {vat_percent} must be >= 0
3. {vat_included} must be bool
"""


@pytest.mark.parametrize("money, vat_percent, vat_included, is_positive_test",
                         [(-1, 5, True, False),
                          (5, -1, False, False),
                          (0, 20, False, True),
                          (50, 0, True, True),
                          (2147483650, 5, True, False),
                          (10, 2147483647, True, True)]
                         )
def test_tax_with_boundary_values(money, vat_percent, vat_included, is_positive_test):
    data = {'money_sum': money, 'vat_percent': vat_percent, 'vat_included': vat_included}

    if is_positive_test:
        actual_result = vat_calculate(data)
        expected_result = vat_assertion(data)
    else:
        actual_result = vat_calculate(data).get('message')
        expected_result = Errors().invalid_data

    assert expected_result == actual_result


"""
#3 Check invalid values for all fields in vat_calculate

Steps:
1. {money} must be int or float
2. {vat_percent} must be int or float
3. {vat_included} must be bool
"""


def test_vat_calculate_bad_request(types_pairwise):
    data = {'money_sum': types_pairwise[0], 'vat_percent': types_pairwise[1], 'vat_included': types_pairwise[2]}

    actual_result = vat_calculate(data).get('message')
    expected_result = Errors().invalid_data

    assert expected_result == actual_result


"""
#4 Check structure of response

Steps:
1. Call function with positive data
2. Send data of response and schema to check_structure

Expected result:
return of function check_structure must be True

"""


def test_vat_calculate_check_structure_of_response():
    data = {'money_sum': 50, 'vat_percent': 2, 'vat_included': True}

    response = vat_calculate(data)
    schema = {'total': 'str', 'vat': 'str',
              'total_without_vat': 'str'}

    actual_result = check_structure(response.get('data'), schema)
    expected_result = True

    assert expected_result == actual_result
